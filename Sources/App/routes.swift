import Vapor

/// Register your application's routes here.
public func routes(_ app: Application) throws {

    try app.register(collection: SensorController())
    try app.register(collection: ReadingController())
    try app.register(collection: WebsiteController())
}
