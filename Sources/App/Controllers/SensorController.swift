//
//  SensorController.swift
//  App
//
//  Created by Axel Roest on 22-02-2020.
//

import Vapor

/// Shows a list of the available sensors
struct SensorController: RouteCollection {
    
    func boot(routes: RoutesBuilder) throws {
        let sensorRoutes = routes.grouped("sensors")

        sensorRoutes.post(use: create)
        sensorRoutes.get(use: index)
        sensorRoutes.delete(":sensorID", use: delete)
        sensorRoutes.get("setup", use: setup)
    }
   
    func setup(_ req: Request) throws -> String  {
        let sensor1 = Sensor(id: 1, name: "Sansevieria", imageUrl:"http://plants.roeroe.nl:8080/images/sansevieria.jpg", tooDry: 200.0)
        let sensor2 = Sensor(id: 2, name: "Aloë Vera", imageUrl:"http://plants.roeroe.nl:8080/images/aloevera.jpg", tooDry: 400.0)
        _ = sensor1.create(on: req.db)
        _ = sensor2.create(on: req.db)
        return "ok"
    }
    
    /// Returns a list of all `Sensor`s.
    func index(_ req: Request) throws -> EventLoopFuture<[Sensor]> {
        Sensor.query(on: req.db).all()
    }

    func create(_ req: Request) throws -> EventLoopFuture<Sensor> {
        let sensor = try req.content.decode(Sensor.self)
        
        return sensor.save(on: req.db)
            .map { sensor }
    }

    func delete(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        
        Sensor.find(req.parameters.get("sensorID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { sensor in
                sensor.delete(on: req.db)
                    .transform(to: .noContent)
            }
    }
}
