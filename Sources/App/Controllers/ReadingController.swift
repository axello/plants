//
//  ReadingController.swift
//  App
//
//  Created by Axel Roest on 22-02-2020.
//

import Vapor

/// Shows a list of the available sensors
struct ReadingController: RouteCollection {

    func boot(routes: RoutesBuilder) throws {
        let readingRoutes = routes.grouped("readings")

        readingRoutes.get(use: index)
        readingRoutes.delete(":readingID", use: delete)
        readingRoutes.get("setup", use: setup)
    }
    
    func setup(_ req: Request) throws -> String {
        for n in 1...10 {
            let dn = Double(n)
            let reading1 = Reading(id: n, sensorID: 1, date: Date().addingTimeInterval(60.0 * dn), value: 320.0 + dn)
            let reading2 = Reading(id: n*2, sensorID: 2, date: Date().addingTimeInterval(60.0 * dn), value: 340.0 - dn)
            _ = reading1.create(on: req.db)
            _ = reading2.create(on: req.db)
        }
        return "ok"
    }

    /// Returns a list of all Readings.
    func index(_ req: Request) throws -> EventLoopFuture<[Reading]> {
        Reading.query(on: req.db).all()
    }

    func create(_ req: Request) throws -> EventLoopFuture<Reading> {
        let reading = try req.content.decode(Reading.self)
        return reading.save(on: req.db)
            .map { reading }
    }

    func delete(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        Reading.find(req.parameters.get("readingID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { reading in
                reading.delete(on: req.db)
                    .transform(to: .ok)
            }
    }
}
