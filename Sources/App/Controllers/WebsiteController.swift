//
//  WebsiteController.swift
//  
//
//  Created by Axel Roest on 17-06-2022.
//

import Vapor
import Leaf
import Fluent
import Dispatch

struct WebsiteController: RouteCollection {
    struct ViewSensor: Encodable {
        var id: Int
        var name: String
        var status: String
        var image: String
    }
    struct HomeContext: Encodable {
        let sensors: [ViewSensor]
    }

    func boot(routes: RoutesBuilder) throws {
        // 3
        routes.get(use: indexHandler)
        routes.get("setup", use: setupHandler)
    }

    func indexHandler(_ req: Request) -> EventLoopFuture<View> {
        req.eventLoop.flatSubmit { () -> EventLoopFuture<View> in
            Sensor.query(on: req.db)
                .all()
                .flatMapEachCompact(on: req.eventLoop) { sensor in
                    return Reading.query(on: req.db)
                        .filter(\.$sensor.$id == sensor.id!)
                        .sort(\.$date, .descending)
                        .first()
                        .optionalMap { lastReading -> ViewSensor in
                            let status = sensor.status(lastReading.value)
                            let plantImage = sensor.image ?? "https://webstockreview.net/images/flower-pot-png-2.png"
                            let sensorContext = ViewSensor(id: sensor.id!, name: sensor.name, status: status.status, image: plantImage)
                            return sensorContext
                        }
                }
                .flatMap { readings -> EventLoopFuture<View> in
                    let plants = HomeContext(sensors: readings)
                    return req.view.render("home", plants)
                }
        }
    }
    
    func setupHandler(_ req: Request) -> EventLoopFuture<View> {
        let sensor1 = Sensor(id: 1, name: "Sansevieria", imageUrl:"http://plants.roeroe.nl:8080/images/sansevieria.jpg", tooDry: 200.0)
        let sensor2 = Sensor(id: 2, name: "Aloë Vera", imageUrl:"http://plants.roeroe.nl:8080/images/aloevera.jpg", tooDry: 400.0)
        _ = sensor1.create(on: req.db)
        _ = sensor2.create(on: req.db)
        
        return Sensor.query(on: req.db).all()
            .flatMap { sensors in
                for sensor in sensors {
                    // fetched sensors are guaranteed to have an id
                    let sensorId = sensor.id!
                    for n in 1...10 {
                        let dn = Double(n)
                        let value = sensorId == 1 ? sensor.drySetpoint + 100 + dn : sensor.drySetpoint - 2 * dn
                        let reading = Reading(sensorID: sensorId, date: Date().addingTimeInterval(60.0 * dn), value: value)
                        _ = reading.create(on: req.db)
                    }
                }
                return indexHandler(req)
            }
    }
    
}
