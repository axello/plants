import Fluent

struct CreateReading: Migration {
    // 2
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        // 3
        database.schema("readings")
            // 4
            .field("id", .int, .identifier(auto: false))
            // 5
            .field("sensor", .int32, .required)
            .field("date", .datetime, .required)
            .field("value", .double, .required)
            // 6
            .create()
    }
    
    // 7
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("readings").delete()
    }
}
