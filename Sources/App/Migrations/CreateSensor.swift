import Fluent

struct CreateSensor: Migration {
    // 2
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        // 3
        database.schema("sensors")
            // 4
            .field("id", .int, .identifier(auto: false))
            // 5
            .field("name", .string, .required)
            .field("drySetpoint", .double, .required)
            .field("image", .string)
            // 6
            .create()
    }
    
    // 7
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("sensors").delete()
    }
}
