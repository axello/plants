import FluentSQLiteDriver
import Vapor
import Leaf
import COperatingSystem


/// Called before your application initializes.
///
public func configure(_ app: Application) throws {
    
    
    let directoryConfig = DirectoryConfiguration.detect()
//    services.register(directoryConfig)                  // services cannot throw

    app.databases.use(.sqlite(.file("\(directoryConfig.workingDirectory)readings.sqlite3")), as: .sqlite)
    print("working directory database: \(directoryConfig.workingDirectory)readings.sqlite3")
    app.views.use(.leaf)
    app.middleware.use(FileMiddleware(publicDirectory: app.directory.publicDirectory))

    app.logger.logLevel = .debug

    // Configure migrations
    app.migrations.add(CreateSensor())
//    app.migrations.add(CreateReading())
    
    try app.autoMigrate().wait()
    
    try routes(app)

}

//public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
//    // Register providers first
//    try services.register(FluentSQLiteProvider())
//
//    // Register routes to the router
//    let router = EngineRouter.default()
//    try routes(router)
//    services.register(router, as: Router.self)
//
//    // Register middleware
//    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
//    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
//    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
//    services.register(middlewares)
//
//    let directoryConfig = DirectoryConfig.detect()
//    services.register(directoryConfig)                  // services cannot throw
//
//    // Configure a SQLite database
////    let sqlite = try SQLiteDatabase(storage: .memory)
//    var databaseConfig = DatabasesConfig()
//    let db = try SQLiteDatabase(storage: .file(path: "\(directoryConfig.workDir)readings.sqlite3"))
//    databaseConfig.add(database: db, as: .sqlite)
//    services.register(databaseConfig)
//
//    try services.register(LeafProvider())
//    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
//
//    // Configure migrations
//    var migrations = MigrationConfig()
//    migrations.add(model: Sensor.self, database: DatabaseIdentifier<Sensor.Database>.sqlite)
//    migrations.add(model: Reading.self, database: DatabaseIdentifier<Reading.Database>.sqlite)
//    services.register(migrations)
//}
