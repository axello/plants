//
//  Sensor.swift
//  App
//
//  Created by Axel Roest on 22-02-2020.
//

import FluentSQLiteDriver
import Vapor

enum SensorStatus {
    case unknown
    case perfect
    case tooDry
    
    var status: String {
        switch self {
        case .unknown:
            return "❓"
        case .perfect:
            return "Happy 😋"
        case .tooDry:
            return "I'm thirsty!🚰"
        }
    }
}

final class Sensor: Model, Content {
    
    static var schema = "sensors"
    
    @ID(custom: .id)
    var id: Int?

    @Field(key: "name")
    var name: String

    @Field(key: "drySetpoint")
    var drySetpoint: Double

    @Field(key: "image")
    var image: String?
    
    @Children(for: \.$sensor)
    var readings: [Reading]
    
    init() {}

    init(id: Int? = nil, name: String, imageUrl: String? = nil, tooDry: Double) {
        self.id = id
        self.name = name
        self.image = imageUrl
        self.drySetpoint = tooDry
    }
    
    func status(_ value: Double?) -> SensorStatus {
        guard let result = value else {
            return .unknown
        }
        if result < drySetpoint {
            return .tooDry
        }
        return .perfect
    }

}
