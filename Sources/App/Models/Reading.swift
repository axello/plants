//
//  Reading.swift
//  App
//
//  Created by Axel Roest on 22-02-2020.
//

import Foundation
//import Fluent
import FluentSQLiteDriver
import Vapor

final class Reading: Model, Content {

    static let schema = "readings"

    @ID(custom: .id)
    var id: Int?

    @Parent(key: "sensor")
    var sensor: Sensor

    @Timestamp(key: "date", on: .none, format: .unix)
    var date: Date?

    @Field(key: "value")
    var value: Double
    
//    dynamic var dateValue: Date {
//        get {
//            let realDate = dateFormatter.date(from: date)
//            assert(realDate != nil)
//            return realDate!
//        }
//        set {
//            self.date = dateFormatter.string(from: newValue)
//        }
//    }
    
    private static let dateFormatter = DateFormatter()

    init() {}

    init(id: Int? = nil, sensorID: Int, date: Date, value: Double ) {
        self.id = id
        self.$sensor.id = sensorID
        self.value = value
        self.date = date     // dateFormatter.string(from: date)
    }
    
    convenience init(id: Int? = nil, sensorID: Int, date: String, value: Double ) {
//        let dateFormatter = DateFormatter()
        Reading.dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"            // compatible with sqlite3 injection
        let realDate = Reading.dateFormatter.date(from: date) ?? Date()
        self.init(id: id, sensorID: sensorID, date: realDate, value: value)
    }

}
